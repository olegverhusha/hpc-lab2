#!/bin/bash
#PBS -N count_processes
#PBS -l nodes=1:ppn=1
#PBS -l walltime=00:01:00

# Зміна робочого каталогу на каталог, звідки була подана задача
cd $PBS_O_WORKDIR

# Каталог для збереження результату підрахунку
OUTPUT_DIR=$PBS_O_WORKDIR/lab2_output

# Declare an associative array to store the count of processes on each node
declare -A PROCESS_COUNT

# Iterate over the nodefiles for all tasks to count the number of processes executed on each node
for (( i = 1; i <= 100; i++ )); do
    while read -r NODE; do
        if [[ -n "$NODE" && "$NODE" != ":" ]]; then
            PROCESS_COUNT["$NODE"]=$((PROCESS_COUNT["$NODE"] + 1))
        fi
    done < $OUTPUT_DIR/nodefile_task_$i.out
done

# Output the result to a file
echo "Node-wise process count:" > $OUTPUT_DIR/process_count.out
for NODE in "${!PROCESS_COUNT[@]}"; do
    echo "$NODE: ${PROCESS_COUNT[$NODE]}" >> $OUTPUT_DIR/process_count.out
done
