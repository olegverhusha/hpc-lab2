#!/bin/bash
#PBS -N mpi_hello_job
#PBS -l nodes=2:ppn=2
#PBS -l walltime=00:02:00
#PBS -j oe
#PBS -t 1-100

# Завантаження оточення компілятора та MPI
ml icc
ml openmpi

# Зміна робочого каталогу на каталог, звідки була подана задача
cd $PBS_O_WORKDIR

# Каталог, де знаходяться ваші виконувані файли
EXECUTABLE_FILE=mpi_program

# Локальний каталог на вузлах
LOCAL_DIR=$PBS_O_HOME/tmp

# Каталог для збереження стандартного виводу
OUTPUT_DIR=$PBS_O_WORKDIR/lab2_output

# Створення локального каталогу
mkdir -p $LOCAL_DIR

# Створення вихідного каталогу
mkdir -p $OUTPUT_DIR

# Копіювання виконуваних файлів у локальний каталог
cp $PBS_O_WORKDIR/$EXECUTABLE_FILE $LOCAL_DIR

# Переміщення у локальний каталог
cd $LOCAL_DIR

# Запуск MPI програми
mpirun $EXECUTABLE_FILE > $OUTPUT_DIR/output_$PBS_ARRAYID.out

# Очищення локального каталогу
rm -rf $LOCAL_DIR

# Очікування завершення усіх MPI програм перед підрахунком
wait

# Зберігання інформації про використані вузли для цієї задачі
echo "Task $PBS_ARRAYID Nodefile:" > $OUTPUT_DIR/nodefile_task_$PBS_ARRAYID.out
cat $PBS_NODEFILE >> $OUTPUT_DIR/nodefile_task_$PBS_ARRAYID.out